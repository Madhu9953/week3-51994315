package assignmentWeek3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MagicOfBooks {
  
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int k=0;
		ArrayList<BookClass> bookdata = new ArrayList<BookClass>();
		BookClass b1=new BookClass("Panchatantra",400,"stories",5);
		
		BookClass b2=new BookClass("Bhatti-Vikramarka",2500,"stories",7);
		
		BookClass b3=new BookClass("Geetanjali",3000,"poetry",8);
		
		BookClass b4=new BookClass("Bose",200,"Auto-biography",9);
	
		bookdata.add(b1);bookdata.add(b2);bookdata.add(b3);bookdata.add(b4);
		for(BookClass b: bookdata) {
		System.out.println("Book name:"+b.Name+"Book price:"+b.Price+ "book Genre:"+b.Genre+ "No.of copies sold"+b.noOfCopyesSold); }
	
		
		//adding  new book
		BookClass b5=new BookClass("BhagatSingh",150,"Auto-biography",10);
		bookdata.add(b5);
		
		System.out.println("Data after adding the book;");
		for(BookClass bo: bookdata) {
			System.out.println("Book name:"+bo.Name+"Book price:"+bo.Price+ "book Genre:"+bo.Genre+ "No.of copies sold"+bo.noOfCopyesSold); }
		//removing book (b2)
		bookdata.remove(b5);
		System.out.println("Data after deleting the book;");
		for(BookClass boo: bookdata) {
			System.out.println("Book name:"+boo.Name+"Book price:"+boo.Price+ "book Genre:"+boo.Genre+ "No.of copies sold"+boo.noOfCopyesSold); }

		
		bookdata.sort(Comparator.comparing(BookClass::getPrice));
		System.out.println("Order of book in increasing order of price:");
		for(BookClass e :bookdata) {
			System.out.println("Book name:"+e.Name+"Book price:"+e.Price+ "book Genre:"+e.Genre+ "No.of copies sold"+e.noOfCopyesSold); 
			}
		System.out.println("Order of book in decreasing order of price:");
		Comparator<BookClass> comparator = Comparator.comparing(e -> e.getPrice());
		bookdata.sort(comparator.reversed());
		for(BookClass em :bookdata) {
			System.out.println("Book name:"+em.Name+"Book price:"+em.Price+ "book Genre:"+em.Genre+ "No.of copies sold"+em.noOfCopyesSold); }
			
		try {
		for(BookClass lib: bookdata) {
			if(lib.Genre.equals("Auto-biography")==true) {
				k++;
			}}System.out.println("no.of Autobiographies" +k);}
			catch(Exception e) {
				System.out.println("Not found anything");}
		
		
		 //Optional<BookClass> bookdata1 = bookdata.stream().collect(Collectors.maxBy(Comparator.comparing(BookClass::getNoOfCopyesSold)));
		ArrayList<String> b11 = new ArrayList<String>();
	bookdata.sort(Comparator.comparing(BookClass::getNoOfCopyesSold));
	for(BookClass b: bookdata) {
		b11.add(b.Name);
		}
	System.out.println("Highly sold book:"+ b11.get(0));
		
		
		
		
		Map<Integer, String> library01 = new HashMap<Integer, String>();
		library01.put(1, "Panchatantra");
		library01.put(2, "Bhatti-Vikramarka");
		library01.put(3, "Geetanjali");
		library01.put(4, "Bose");
        System.out.println(library01);
		}	
	}
